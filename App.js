import React from "react";
import "./App.css";
import UseSetState from "./UseSetState";
function App() {
  const [customState, setcustomState] = UseSetState({
    str: "",
    int: 0,
    bool: false,
  });

  return (
    <div className="App">
      <p>My awsome text: {customState.str}</p>
      <input
        type="text"
        onChange={(e) => setcustomState({ str: e.target.value })}
      />
      <p>Clicked {customState.int} times! </p>
      <input
        type="submit"
        value="increment"
        onClick={() =>
          setcustomState((state) => {
            return { int: state.int + 1 };
          })
        }
      />
      <input
        type="checkbox"
        onChange={(e) => setcustomState({ bool: e.target.checked })}
      />
      <input
        type="submit"
        value="log state"
        onClick={() => console.log(customState)}
      />
    </div>
  );
}

export default App;
