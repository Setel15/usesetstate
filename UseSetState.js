import { useState, useEffect } from "react";
import * as _ from "lodash";

export default function useSetState(initialState) {
  let [state, setState] = useState(initialState);

  function setPartialState(arg = {}, callback = () => {}) {
    if (_.isFunction(arg) && _.isObject(arg(state))) {
      setState({ ...state, ...arg(state) });
    } else if (_.isObject(arg)) {
      setState({ ...state, ...arg });
    } else {
      throw new Error("function get an object or callback as a parameter");
    }
    useEffect(callback(state), [state]);
  }

  if (!_.isObject(state)) {
    throw new Error("given state should be an object");
  }

  return [state, setPartialState];
}
